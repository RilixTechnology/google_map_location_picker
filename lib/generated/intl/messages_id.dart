// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a id locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'id';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "access_to_location_denied":
            MessageLookupByLibrary.simpleMessage("Akses Lokasi ditolak"),
        "access_to_location_permanently_denied":
            MessageLookupByLibrary.simpleMessage(
                "Akses Lokasi ditolak permanen"),
        "allow_access_to_the_location_services":
            MessageLookupByLibrary.simpleMessage(
                "Perbolehkan akses untuk layanan lokasi."),
        "allow_access_to_the_location_services_from_settings":
            MessageLookupByLibrary.simpleMessage(
                "Perbolehkan akses ke layanan lokasi untuk Aplikasi ini melalui pengaturan perangkat."),
        "cant_get_current_location": MessageLookupByLibrary.simpleMessage(
            "Tidak bisa mendapatkan lokasi saat ini"),
        "finding_place":
            MessageLookupByLibrary.simpleMessage("Menemukan tempat..."),
        "no_result_found":
            MessageLookupByLibrary.simpleMessage("Tidak ditemukan hasil"),
        "ok": MessageLookupByLibrary.simpleMessage("Ok"),
        "please_check_your_connection": MessageLookupByLibrary.simpleMessage(
            "Mohon periksa koneksi internet Anda"),
        "please_make_sure_you_enable_gps_and_try_again":
            MessageLookupByLibrary.simpleMessage(
                "Mohon pastikan Anda telah menyalakan GPS dan coba lagi"),
        "search_place": MessageLookupByLibrary.simpleMessage("Cari tempat"),
        "server_error":
            MessageLookupByLibrary.simpleMessage("Kesalahan server"),
        "unnamedPlace":
            MessageLookupByLibrary.simpleMessage("Tempat tak bernama")
      };
}
